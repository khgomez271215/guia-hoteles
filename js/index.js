$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:4000
    });

    $('#contactoMdl').on('show.bs.modal',function(e){
        console.log('el modal contacto se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled',true);
    });

    $('#contactoMdl').on('shown.bs.modal',function(e){
        console.log('el modal contacto se mostro');
    });

    $('#contactoMdl').on('hide.bs.modal',function(e){
        console.log('el modal contacto se oculta');
    });

    $('#contactoMdl').on('hidden.bs.modal',function(e){
        console.log('el modal contacto se oculto');
        $('#contactoBtn').prop('disabled',false);
        $('#contactoBtn').addClass('btn-success');
    });
});